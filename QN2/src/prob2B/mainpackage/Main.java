package prob2B.mainpackage;

import prob2B.Order;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args){
        Order order1 = new Order(1, LocalDate.now());

        order1.addOrderLine(1,345.67);
        order1.addOrderLine(2,432.00);

        System.out.println(order1.getOrderLines());
    }
}
