package prob2B;

public class OrderLine {
    private int lineNum;
    private double price;
    private Order order;

    OrderLine(int lineNum, double price, Order order){
        this.lineNum = lineNum;
        this.price = price;
        this.order = order;
    }

    @Override
    public String toString(){
        return "Line Num: "+ lineNum +" Price: "+price;
    }
}
