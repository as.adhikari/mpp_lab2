package prob2B;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {
    private int orderNum;
    private LocalDate orderDate;
    private List<OrderLine> orderLines;

    public Order(int orderNum, LocalDate orderDate){
        this.orderNum = orderNum;
        this.orderDate = orderDate;
        orderLines = new ArrayList<>();
    }

    public void addOrderLine(int lineNum, double price){
        orderLines.add(new OrderLine(lineNum,price,this));
    }

    public List<OrderLine> getOrderLines(){
        return orderLines;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

}
