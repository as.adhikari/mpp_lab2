package prob2A.mainpackage;

import prob2A.Student;

public class Main {

    public static void main(String[] args){

        Student student1 = new Student("Ashok");
        student1.addGrade(3.47);

        System.out.println(student1.getGradeReport());
    }
}
