package prob2A;

public class GradeReport {
    private Student student;
    private double grade;

    GradeReport(Student student){
        this.student = student;
    }

    public void addGrade(double grade){
        this.grade = grade;
    }

    @Override
    public String toString(){
        return ""+grade;
    }
}
