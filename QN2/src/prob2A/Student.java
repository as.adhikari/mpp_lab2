package prob2A;

public class Student {
    private String name;
    private GradeReport gradeReport;

    public Student(String name){
        this.name = name;
        gradeReport = new GradeReport(this);

    }

    public void addGrade(double grade){
        this.gradeReport.addGrade(grade);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GradeReport getGradeReport(){
        return this.gradeReport;
    }

}
