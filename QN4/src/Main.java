import java.util.ArrayList;
import java.util.List;

public class Main {
    Student bob;
    Student tim;
    Student allen;
    List<Student> students;
    Section bio1;
    Section bio2;
    Section math;

    public static void main(String[] args) {
        Main m = new Main();
        m.readDataFromDb();
        System.out.println(m.getTranscript(m.bob));
        System.out.println("Grades for math section:\n " + m.getGrades(m.math));
        System.out.println("Courses that Tim took: " + m.getCourseNames(m.tim));
        System.out.println("Students who got A's: " + m.getStudentsWith("A"));
    }

    private Transcript getTranscript(Student s) {
        return s.getTranscript();
    }

    private List<String> getCourseNames(Student s) {
        List<TranscriptEntry> all = s.grades;
        List<String> courseNames = new ArrayList<>();
        for (TranscriptEntry te : all) {
            courseNames.add(te.section.courseName);
        }
        return courseNames;
    }

    private List<String> getGrades(Section s) {
        List<String> grades = new ArrayList<>();
        for (TranscriptEntry t : s.gradeSheet) {
            grades.add(t.grade);
        }
        return grades;
    }

    private List<String> getStudentsWith(String grade) {
        List<String> studentNames = new ArrayList<>();
        for (Student s : students) {
            boolean found = false;
            for (TranscriptEntry te : s.grades) {
                if (!found) {
                    if (te.grade.equals(grade)) {
                        found = true;
                        studentNames.add(s.name);
                    }
                }
            }
        }
        return studentNames;
    }

    private void readDataFromDb() {

        students = new ArrayList<>();
        bob = StudentSectionFactory.createStudent("ID1", "Bob");
        tim = StudentSectionFactory.createStudent("ID2", "Tim");
        allen = StudentSectionFactory.createStudent("ID3", "Allen");

        students.add(bob);
        students.add(tim);
        students.add(allen);

        bio1 = StudentSectionFactory.createSection(1, "Biology 1");
        bio2 = StudentSectionFactory.createSection(2, "Biology 2");
        math = StudentSectionFactory.createSection(3, "Mathematics");

        StudentSectionFactory.newTranscriptEntry(bob, bio1, "A");
        StudentSectionFactory.newTranscriptEntry(bob, math, "B");
        StudentSectionFactory.newTranscriptEntry(tim, bio1, "B+");
        StudentSectionFactory.newTranscriptEntry(tim, math, "A-");
        StudentSectionFactory.newTranscriptEntry(allen, math, "B");
        StudentSectionFactory.newTranscriptEntry(allen, bio2, "B+");

    }
}