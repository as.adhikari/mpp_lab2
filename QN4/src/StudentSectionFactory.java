
public class StudentSectionFactory {

    public static Section createSection(int secNum, String courseName){
        return new Section(secNum, courseName);
    }

    public static Student createStudent(String id, String name){
        return new Student(id, name);
    }

    public static void newTranscriptEntry(Student student, Section section, String grade){
        TranscriptEntry newTranscriptEntry = new TranscriptEntry(student, section, grade);
        student.addTranscriptEntry(newTranscriptEntry);
        section.addTranscriptEntry(newTranscriptEntry);

    }
}
