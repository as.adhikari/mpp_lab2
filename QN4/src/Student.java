import java.util.ArrayList;
import java.util.List;

public class Student {
    String name;
    String id;
    List<TranscriptEntry> grades = new ArrayList<>();

    Student(String id, String name){
        this.id = id;
        this.name = name;
    }

    public Transcript getTranscript() {
        return new Transcript(grades, this);

    }

    public void addTranscriptEntry(TranscriptEntry transcriptEntry){
        grades.add(transcriptEntry);
    }
}
