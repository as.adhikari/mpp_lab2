import java.util.ArrayList;
import java.util.List;

public class Section {
    String courseName;
    int sectionNumber;
    List<TranscriptEntry> gradeSheet = new ArrayList<>();

    Section(int sectionNumber, String courseName){
        this.sectionNumber = sectionNumber;
        this.courseName = courseName;
    }

    public void addTranscriptEntry(TranscriptEntry transcriptEntry){
        gradeSheet.add(transcriptEntry);
    }
}
